# frozen_string_literal: true

class ModalComponentPreview < ViewComponent::Preview
  # Modal
  # ---------------
  # This is the modal component.
  def default; end
  def dismissible; end
end
