# frozen_string_literal: true

class ButtonComponentPreview < ViewComponent::Preview
  # Button
  # ---------------
  def default; end

  def icon_button; end
end
