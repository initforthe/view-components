# frozen_string_literal: true

class SlideoverComponentPreview < ViewComponent::Preview
  # Slideover
  # ---------------
  # This is the slideover component.
  def default; end

  def secondary; end

  def plain; end

  def without_overlay; end
end
