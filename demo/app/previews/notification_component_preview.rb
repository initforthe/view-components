# frozen_string_literal: true

class NotificationComponentPreview < ViewComponent::Preview
  # Notification
  # ---------------
  # This is a notification.
  def default; end

  def static; end

  def dismissible; end
end
