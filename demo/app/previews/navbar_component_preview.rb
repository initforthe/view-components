# frozen_string_literal: true

class NavbarComponentPreview < ViewComponent::Preview
  # Main Navigation
  # ---------------
  # This is the main navigation component. It is used to render the main navigation of the application.
  #
  # @param notification_count number "The number of notifications to display in the notification badge."
  def default(notification_count: nil)
    @user = User.new(name: 'John Doe', email: 'john@doe.com')
    @notification_count = notification_count
  end
end
