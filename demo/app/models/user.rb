# frozen_string_literal: true

class User
  include ActiveModel::Model
  include ActiveModel::Attributes

  attribute :name, :string
  attribute :email, :string
  attribute :hobbies, array: true, default: []
end
