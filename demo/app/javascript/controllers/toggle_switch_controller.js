import { Controller } from "@hotwired/stimulus"

export default class ToggleSwitch extends Controller {
  static targets = ['button', 'element']
  static values = {
    initial: Boolean,
    checked: String,
    unchecked: String,
    buttonEnabledClasses: String,
    buttonDisabledClasses: String,
    spanEnabledClasses: String,
    spanDisabledClasses: String,
  }

  initialize() {
    this.enabled = Boolean(this.initialValue)
    this.enabled ? this.toggleOn() : this.toggleOff()
  }

  getSpan() {
    return this.buttonTarget.querySelector('span')
  }

  toggle(e) {
    this.enabled = !this.enabled
    this.enabled ? this.toggleOn() : this.toggleOff()
    this.dispatchEvent()
  }

  toggleOn() {
    this.buttonTarget.classList.replace(this.buttonDisabledClassesValue, this.buttonEnabledClassesValue)
    this.getSpan().classList.replace(this.spanDisabledClassesValue, this.spanEnabledClassesValue)
    this.elementTarget.setAttribute('checked', 'checked')
  }

  toggleOff() {
    this.buttonTarget.classList.replace(this.buttonEnabledClassesValue, this.buttonDisabledClassesValue)
    this.getSpan().classList.replace(this.spanEnabledClassesValue, this.spanDisabledClassesValue)
    this.elementTarget.removeAttribute('checked')
  }

  dispatchEvent() {
    const event = new CustomEvent('toggle:changed', {
      detail: { enabled: this.enabled }, // Custom data to pass with the event
      bubbles: true,                     // Event will bubble up the DOM
      cancelable: true                   // Allows the event to be cancelable
    })

    this.elementTarget.dispatchEvent(event)
  }
}
