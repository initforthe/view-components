# Pin npm packages by running ./bin/importmap

pin 'application', preload: true
pin '@hotwired/turbo-rails', to: 'turbo.min.js', preload: true
pin '@hotwired/stimulus', to: 'stimulus.min.js', preload: true
pin '@hotwired/stimulus-loading', to: 'stimulus-loading.js', preload: true
pin_all_from 'app/javascript/controllers', under: 'controllers'
pin 'stimulus-existence', to: 'https://ga.jspm.io/npm:stimulus-existence@1.1.5/dist/stimulus-existence.esm.js'
pin 'stimulus-reveal', to: 'https://ga.jspm.io/npm:stimulus-reveal@1.4.2/dist/stimulus-reveal.esm.js'
pin 'trix', to: 'https://ga.jspm.io/npm:trix@2.0.5/dist/trix.esm.min.js'
pin '@rails/actiontext', to: 'actiontext.js'
