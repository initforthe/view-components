# frozen_string_literal: true

require 'initforthe/view_components/version'
require 'initforthe/view_components/engine'

require 'view_component'
require 'heroicons'
require 'font_awesome5_rails'
require 'config'

module Initforthe
  # Initforthe ViewComponents
  module ViewComponents
    # Your code goes here...
  end
end
