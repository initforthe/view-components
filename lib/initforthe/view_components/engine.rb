# frozen_string_literal: true

module Initforthe
  module ViewComponents
    # Main Engine
    class Engine < ::Rails::Engine
      config.autoload_paths = %W[
        #{root}/app/components
        #{root}/app/helpers
      ]

      config.to_prepare do
        ActionView::Base.field_error_proc = ->(html_tag, _instance) { html_tag.html_safe }
        ApplicationController.helper(ViewHelper)
      end

      initializer 'config' do
        raise('the `config` gem needs to be initialized before this gem') unless defined?(Settings)

        Settings.prepend_source!("#{root}/config/settings.yml")
        Settings.reload!
      end
    end
  end
end
