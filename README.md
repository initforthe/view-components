# InitfortheViewComponents
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem "initforthe_view_components"
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install initforthe_view_components
```

## Dependencies
Add Stimulus controllers to `app/javascript/controllers/index.js` :

```js
import RevealController from 'stimulus-reveal'
application.register('reveal', RevealController)
import ExistenceController from 'stimulus-existence'
application.register('existence', ExistenceController)
```

If you are using the the toggle switch component, copy the toggle switch stimulus controller from
`demo/app/javascript/controllers/toggle_switch_controller.js`
to your javascript controllers folder, and make sure it is loaded into your application.

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
