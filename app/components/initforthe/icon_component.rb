# frozen_string_literal: true

module Initforthe
  # Icon Component
  class IconComponent < ViewComponent::Base
    def initialize(icon:, source: nil, variant: nil, size: nil, classes: '', html: {}) # rubocop:disable Metrics/ParameterLists
      @icon = icon
      @source = source
      @variant = variant
      @size = size
      @classes = classes
      @html = html
      super
    end

    def call
      case default_source
      when :heroicon
        helpers.heroicon(@icon, variant: default_variant, options: @html.merge(class: classes))
      when :font_awesome
        helpers.fa_inline_icon(@icon, type: default_variant, **@html.merge(class: classes))
      end
    end

    private

    def default_source
      @source || :heroicon
    end

    def default_variant
      return @variant if @variant

      case default_source
      when :heroicon then :outline
      when :font_awesome then :solid
      end
    end

    def classes
      classes = []
      classes << default_size
      classes << @html[:class]
      classes << @classes
      classes.compact.join(' ')
    end

    def default_size
      @size || Settings.components.icon.sizes.dig(default_source, default_variant) || 'w-5 h-5'
    end
  end
end
