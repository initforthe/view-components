# frozen_string_literal: true

# Modal component
module Initforthe
  # Modal component
  class ModalComponent < Initforthe::Component
    attr_reader :actions, :controllers, :dismissible

    def initialize(size: nil, controllers: nil, actions: nil, dismissible: false, overflow: false, # rubocop:disable Metrics/ParameterLists
                   **tag_options)
      super
      @controllers = controllers
      @actions = actions
      @size = size
      @overflow = overflow
      @tag_options = tag_options
      @dismissible = dismissible
    end

    private

    def close_button_styles
      Settings.components.modal.close_button_styles
    end

    def tag_options
      return if @tag_options.blank?

      ActionView::Helpers::TagHelper::TagBuilder.new(view_context).tag_options(@tag_options).html_safe
    end

    def size
      @size || 'sm:max-w-sm'
    end
  end
end
