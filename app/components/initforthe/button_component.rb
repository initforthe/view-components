# frozen_string_literal: true

module Initforthe
  # Button Component
  class ButtonComponent < ViewComponent::Base
    attr_reader :text, :html, :url, :method

    renders_one :icon, 'IconComponent'

    def initialize(text:, size: nil, type: :primary, url: nil, method: :post, html: {}) # rubocop:disable Metrics/ParameterLists
      @text = text
      @type = type
      @html = html
      @size = size
      @url = url
      @method = method
      super
    end

    private

    def button_size_classes
      case @size
      when :xs then 'rounded px-2 py-1 text-xs'
      when :sm then 'rounded px-2 py-1 text-sm'
      when :md then 'rounded-md px-3 py-2 text-sm'
      when :lg then 'rounded-md px-3.5 py-2.5 text-sm'
      else 'rounded-md px-2.5 py-1.5 text-sm'
      end
    end

    def button_base_classes
      'inline-flex items-center gap-x-1.5 font-semibold shadow-sm'
    end

    def button_status_classes
      Settings.components.button[@type]
    end

    # IconComponent for buttons
    class IconComponent < ViewComponent::Base
      include ViewComponent::InlineTemplate

      def initialize(icon:, source: :heroicon, variant: :solid, side: :left)
        @icon = icon
        @source = source
        @variant = variant
        @side = side
        super
      end

      erb_template <<~ERB
        <%= render Initforthe::IconComponent.new(icon: @icon, source: @source, variant: @variant, classes: icon_classes) %>
      ERB

      private

      def icon_classes
        case @side
        when :left then '-ml-0.5 order-0'
        when :right then '-mr-0.5 order-1'
        end
      end
    end
  end
end
