# frozen_string_literal: true

module Initforthe
  # Navbar Component
  class NavbarComponent < Initforthe::Component
    attr_reader :user, :notification_count, :notifications

    renders_one :logo
    renders_one :links
    renders_one :mobile_links
    renders_one :secondary_links
    renders_one :mobile_secondary_links
    renders_one :notification_badge, Initforthe::NotificationBadgeComponent

    def initialize(user:, notification_count: nil, notifications: false)
      @user = user
      @notification_count = notification_count
      @notifications = notifications
      super
    end

    def before_render
      with_notification_badge count: notification_count
    end

    private

    def background_color
      Settings.components.navbar.background_color
    end

    def mobile_button_color
      Settings.components.navbar.button_color
    end

    def mobile_dropdown_color
      Settings.components.navbar.mobile_dropdown_color
    end

    def mobile_secondary_links_divider_color
      Settings.components.navbar.mobile_secondary_links_divider_color
    end

    def notifications_button_classes
      Settings.components.navbar.notifications_button_classes
    end
  end
end
