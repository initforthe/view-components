# frozen_string_literal: true

module Initforthe
  # Flash component
  #
  # The message object can be a string or a hash. If it's a string, it will just be displayed.
  # If it is a hash, it can be used to create complex flash notices.
  #
  # Options:
  #  - message: The message to display
  #  - type: The type of message (:success, :confirm, :info, :notice, :alert, :warning, :danger, :error)
  #  - dismissible: Whether the notification can be dismissed manually
  #
  # Message Options:
  # - user:
  #   - name: The name of the user
  #   - email: The email of the user
  # - title: The message to display
  # - description: The message to display
  # - links: The links to display
  # - buttons: The buttons to display
  #
  # Examples:
  # With a description:
  # flash[:notice] = { title: 'You have been logged in', description: 'You can now access the dashboard' }
  #
  # With a description and a link:
  # flash[:notice] = {
  #   title: 'You have been logged in',
  #   description: 'You can now access the dashboard',
  #   links: [{ text: 'Go to dashboard', url: '/dashboard' }]
  # }
  #
  # With a description and two links:
  # flash[:notice] = {
  #   title: 'You have been logged in',
  #   description: 'You can now access the dashboard',
  #   links: [
  #     { text: 'Go to dashboard', url: '/dashboard' },
  #     { text: 'Go to profile', url: '/profile', status: :secondary }
  #   ]
  # }
  class FlashComponent < Initforthe::Component
    renders_one :icon, Initforthe::IconComponent
    renders_one :avatar, Initforthe::AvatarComponent
    renders_one :heading
    renders_one :description
    renders_many :links, Initforthe::LinkComponent
    renders_many :buttons, Initforthe::ButtonComponent

    attr_reader :message, :type, :dismissible, :auto_dismiss
    alias dismissible? dismissible
    alias auto_dismiss? auto_dismiss

    def initialize(message:, type: nil, dismissible: false, auto_dismiss: 2.25)
      super
      @message = message
      @type = type&.to_sym
      @dismissible = dismissible
      @auto_dismiss = auto_dismiss
      @user = message[:user] if hash? && message.key?(:user)
    end

    def before_render
      set_icon_or_avatar
      set_text_content
      set_links_and_buttons
    end

    private

    def set_text_content
      with_heading { (hash? ? @message[:title] : @message) }
      with_description { (hash? ? @message[:description] : nil) }
    end

    def set_links_and_buttons
      with_links(@message[:links]) if hash? && @message[:links].present?
      with_buttons(@message[:buttons]) if hash? && @message[:buttons].present?
    end

    def set_icon_or_avatar
      with_avatar(name: @user[:name], email: @user[:email]) if @user
      default_icon_options ? with_icon(**default_icon_options) : nil
    end

    def default_icon_options
      return unless @type

      options = case @type
                when :notice, :info then { icon: 'information-circle' }
                when :success, :confirm then { icon: 'check-circle' }
                when :alert, :warning then { icon: 'exclamation-triangle' }
                when :error, :danger then { icon: 'minus-circle' }
                end
      options&.reverse_merge(source: :heroicon, variant: :outline, classes: default_icon_color_class)
    end

    def default_icon_color_class
      case @type
      when :notice, :info then Settings.components.flash.icon_styles.info
      when :success, :confirm then Settings.components.flash.icon_styles.success
      when :alert, :warning then Settings.components.flash.icon_styles.warning
      when :error, :danger then Settings.components.flash.icon_styles.danger
      end
    end

    def default_classes
      case @type
      when :notice, :info then Settings.components.flash.base_styles.info
      when :success, :confirm then Settings.components.flash.base_styles.success
      when :alert, :warning then Settings.components.flash.base_styles.warning
      when :error, :danger then Settings.components.flash.base_styles.danger
      end
    end

    def hash?
      @message.is_a?(Hash)
    end

    def render?
      @message.present?
    end
  end
end
