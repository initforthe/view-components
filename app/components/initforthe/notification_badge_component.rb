# frozen_string_literal: true

module Initforthe
  # Notification Badge
  class NotificationBadgeComponent < Initforthe::Component
    attr_reader :count

    def initialize(count:)
      @count = count
      super
    end

    private

    def visible?
      @count&.positive?
    end
  end
end
