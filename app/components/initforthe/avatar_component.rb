# frozen_string_literal: true

module Initforthe
  # AvatarComponent
  class AvatarComponent < Component
    def initialize(name:, email:, size: 75, alt: nil, classes: nil)
      @name = name
      @email = email
      @size = size
      @alt = alt || @name
      @classes = classes || 'h-10 w-10 rounded-full'
      super
    end

    def call
      avatar
    end

    private

    def avatar
      image_tag gravatar_url, alt: @alt, class: @classes
    end

    def gravatar_url
      "https://www.gravatar.com/avatar/#{gravatar_hash}?d=#{CGI.escape(fallback_url)}&s=#{@size}"
    end

    def fallback_url
      "https://ui-avatars.com/api/#{CGI.escape(@name)}/128/random"
    end

    def gravatar_hash
      Digest::MD5.hexdigest @email.strip.downcase
    end
  end
end
