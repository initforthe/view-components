# frozen_string_literal: true

module Initforthe
  # Notification Component
  class NotificationComponent < Initforthe::Component
    renders_one :header, lambda { |title:|
      tag.h3(title, class: "text-sm font-medium #{heading_color}")
    }
    renders_one :message
    renders_many :links

    attr_reader :text, :dismissible, :static

    def initialize(type: nil, dismissible: false, static: false, data: nil) # rubocop:disable Lint/MissingSuper
      @type = type&.to_sym
      @dismissible = dismissible
      @static = static
      process_data(data)
    end

    private

    def background_color
      case @type
      when :alert, :warning then 'bg-yellow-50'
      when :notice, :success then 'bg-green-50'
      when :error then 'bg-red-50'
      else 'bg-blue-50'
      end
    end

    def heading_color
      case @type
      when :alert, :warning then 'text-yellow-800'
      when :notice, :success then 'text-green-900'
      when :error then 'text-red-800'
      else 'text-blue-800'
      end
    end

    def text_color
      case @type
      when :alert, :warning then 'text-yellow-700'
      when :notice, :success then 'text-green-800'
      when :error then 'text-red-700'
      else 'text-blue-700'
      end
    end

    # rubocop:disable Layout/LineLength
    def dismissible_colors
      case @type
      when :alert, :warning then 'bg-yellow-50 text-yellow-500 hover:bg-yellow-100 focus:ring-offset-yellow-50 focus:ring-yellow-600'
      when :notice, :success then 'bg-green-50 text-green-500 hover:bg-green-100 focus:ring-offset-green-50 focus:ring-green-600'
      when :error then 'bg-red-50 text-red-500 hover:bg-red-100 focus:ring-offset-red-50 focus:ring-red-600'
      else 'bg-blue-50 text-blue-500 hover:bg-blue-100 focus:ring-offset-blue-50 focus:ring-blue-600'
      end
    end
    # rubocop:enable Layout/LineLength

    def icon
      svg_icon = case @type
                 when :alert, :warning then 'exclamation-triangle'
                 when :notice, :success then 'check-circle'
                 when :error then 'exclamation-circle'
                 else 'information-circle'
                 end
      helpers.heroicon svg_icon, options: { class: "#{icon_class} w-5 h-5" }
    end

    def icon_class
      case @type
      when :alert, :warning then 'text-yellow-400'
      when :notice, :success then 'text-green-400'
      when :error then 'text-red-400'
      else 'text-blue-400'
      end
    end

    def process_data(data)
      case data
      when String
        @text = data
      when Hash
        @text = data[:text]
        with_header title: data[:heading] if data[:heading]
        data[:links]&.each do |link|
          with_link { link }
        end
      end
    end
  end
end
