# frozen_string_literal: true

module Initforthe
  module Fields
    # Rich Text Area Component
    class RichTextAreaComponent < BaseComponent
      def input_class
        "#{super} trix-content prose prose-indigo min-w-full max-w-full focus:ring-1"
      end
    end
  end
end
