# frozen_string_literal: true

module Initforthe
  module Fields
    # Select Component
    class SelectComponent < BaseComponent
      attr_reader :options

      def initialize(f:, name:, options: [], field: {}, html: {}) # rubocop:disable Naming/MethodParameterName
        super(f:, name:, field:, html:)
        @options = options
      end

      private

      def error_classes
        Settings.components.fields.country_select.error_classes
      end

      def normal_classes
        Settings.components.fields.country_select.normal_classes
      end
    end
  end
end
