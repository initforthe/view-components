# frozen_string_literal: true

module Initforthe
  module Fields
    # Check Box Component
    class CheckBoxComponent < BaseComponent
      attr_reader :checked_value, :unchecked_value, :help_text

      def initialize(f:, name:, checked_value: '1', unchecked_value: '0', label: nil, help_text: nil, field: {}, html: {}) # rubocop:disable Naming/MethodParameterName, Metrics/ParameterLists
        super(f:, name:, field:, html:)
        @label = label
        @checked_value = checked_value
        @unchecked_value = unchecked_value
        @help_text = help_text
      end

      def label_identifier
        components = [name]
        components << checked_value if field.merge(html).key?(:multiple)
        components.join('_')
      end

      def input_class
        Settings.components.fields.checkbox.input_classes
      end
    end
  end
end
