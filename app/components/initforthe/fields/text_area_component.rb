# frozen_string_literal: true

module Initforthe
  module Fields
    # Text Area Component
    class TextAreaComponent < BaseComponent
    end
  end
end
