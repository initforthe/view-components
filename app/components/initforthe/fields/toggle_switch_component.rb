# frozen_string_literal: true

module Initforthe
  module Fields
    # Toggle Switch Component
    class ToggleSwitchComponent < CheckBoxComponent
      def button_enabled_classes
        Settings.components.fields.toggle_switch.button_enabled_classes
      end

      def button_disabled_classes
        Settings.components.fields.toggle_switch.button_disabled_classes
      end

      def span_enabled_classes
        Settings.components.fields.toggle_switch.span_enabled_classes
      end

      def span_disabled_classes
        Settings.components.fields.toggle_switch.span_disabled_classes
      end
    end
  end
end
