# frozen_string_literal: true

module Initforthe
  module Fields
    # Select Component
    class CountrySelectComponent < BaseComponent
      attr_reader :options

      def initialize(f:, name:, options: [], field: {}, html: {}) # rubocop:disable Naming/MethodParameterName
        super(f:, name:, field:, html:)
        @options = options
      end

      private

      def error_classes
        Settings.components.fields.select.error_classes
      end

      def normal_classes
        Settings.components.fields.select.normal_classes
      end
    end
  end
end
