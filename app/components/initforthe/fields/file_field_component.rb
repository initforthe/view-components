# frozen_string_literal: true

module Initforthe
  module Fields
    # File Field Component
    class FileFieldComponent < BaseComponent
      private

      def input_class
        'flex-1 block w-full'
      end

      def wrapper_class
        ''
      end
    end
  end
end
