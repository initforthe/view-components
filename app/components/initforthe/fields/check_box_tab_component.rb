# frozen_string_literal: true

module Initforthe
  module Fields
    # Check Box Tab Component
    class CheckBoxTabComponent < CheckBoxComponent
      def input_class
        "#{unchecked_classes} #{checked_classes} #{focused_classes} #{Settings.components.fields.checkbox_tab.base_classes}"
      end

      def focused_classes
        Settings.components.fields.checkbox_tab.focused_classes
      end

      def checked_classes
        Settings.components.fields.checkbox_tab.checked_classes
      end

      def unchecked_classes
        Settings.components.fields.checkbox_tab.unchecked_classes
      end
    end
  end
end
