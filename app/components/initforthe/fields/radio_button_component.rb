# frozen_string_literal: true

module Initforthe
  module Fields
    # Radio Button Component
    class RadioButtonComponent < BaseComponent
      attr_reader :value, :help_text

      renders_one :hint

      def initialize(f:, name:, value:, label_text: nil, help_text: nil, field: {}, html: {}) # rubocop:disable Naming/MethodParameterName, Metrics/ParameterLists
        super(f:, name:, field:, html:)
        @value = value
        @label_text = label_text
        @help_text = help_text
      end

      def label
        @label_text
      end

      def input_class
        Settings.components.fields.radio_button.input_classes
      end
    end
  end
end
