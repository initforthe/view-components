# frozen_string_literal: true

module Initforthe
  module Fields
    # Base Field Component
    class BaseComponent < Initforthe::Component
      attr_reader :f, :name, :field, :html, :label

      def initialize(f:, name:, label: true, field: {}, html: {}) # rubocop:disable Naming/MethodParameterName, Lint/MissingSuper
        @f = f
        @name = name
        @field = field
        @wrapper = html.delete(:wrapper)
        @html = html
        @label = label
      end

      private

      def method_name
        self.class.name.underscore.split('/').last.sub('_component', '')
      end

      def input_class
        [html.delete(:class), base_classes,
         (@f.object&.errors&.key?(@name) ? error_classes : normal_classes)].compact.join(' ')
      end

      def wrapper_class
        @wrapper&.dig(:class)
      end

      def base_classes
        Settings.components.fields.base.base_classes
      end

      def error_classes
        Settings.components.fields.base.error_classes
      end

      def normal_classes
        Settings.components.fields.base.normal_classes
      end
    end
  end
end
