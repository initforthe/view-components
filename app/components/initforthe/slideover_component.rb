# frozen_string_literal: true

module Initforthe
  # Slideover Component
  class SlideoverComponent < ModalComponent
    attr_reader :heading, :overlay

    renders_one :intro

    private

    def initialize(heading:, overlay: true, style: :primary, size: nil, controllers: nil, actions: nil, dismissible: false, overflow: false, # rubocop:disable Metrics/ParameterLists
                   **tag_options)
      super
      @overlay = overlay
      @heading = heading
      @style = style
    end

    def size
      @size || 'max-w-md'
    end

    def heading_background_styles
      Settings.components.slideover.heading.background_styles[@style]
    end

    def heading_text_styles
      Settings.components.slideover.heading.text_styles[@style]
    end

    def heading_intro_styles
      Settings.components.slideover.heading.intro_styles[@style]
    end

    def heading_button_styles
      Settings.components.slideover.heading.button_styles[@style]
    end
  end
end
