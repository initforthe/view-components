# frozen_string_literal: true

module Initforthe
  # LinkComponent
  class LinkComponent < ViewComponent::Base
    attr_reader :text, :url, :html

    def initialize(url:, text: nil, type: :primary, html: {})
      @text = text
      @url = url
      @type = type
      @html = html
      super
    end

    private

    def options
      @html.merge(class: classes)
    end

    def classes
      [link_base_classes, link_status_classes, @html[:class]].compact.flatten.join(' ')
    end

    def link_base_classes
      'transition ease-in-out duration-150'
    end

    def link_status_classes
      Settings.components.link[@type]
    end
  end
end
