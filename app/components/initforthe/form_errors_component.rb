# frozen_string_literal: true

module Initforthe
  # Form Errors Component
  class FormErrorsComponent < NotificationComponent
    def initialize(form_object)
      super(type: :error, dismissible: false, static: true)
      @form_object = form_object
    end

    private

    def before_render
      with_header(title: default_title) unless header
      with_message do
        tag.ul do
          @form_object.errors.full_messages.each do |message|
            concat tag.li(message)
          end
        end
      end
    end

    def render?
      @form_object.errors.any?
    end

    def default_title
      "#{pluralize(@form_object.errors.count,
                   'error')} stopped this #{@form_object.model_name.human.downcase} being saved:"
    end
  end
end
