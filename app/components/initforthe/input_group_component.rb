# frozen_string_literal: true

module Initforthe
  # Input Group component
  class InputGroupComponent < Initforthe::Component
    renders_one :label
    renders_one :hint
    renders_one :inline_hint

    attr_reader :f, :name, :label_text, :show_label

    def initialize(f:, name:, html: {}, label_text: nil, show_label: true) # rubocop:disable Naming/MethodParameterName
      @f = f
      @name = name
      @html = html
      @label_text = label_text
      @show_label = show_label
      super
    end

    private

    def label_classes
      base_classes = ['block text-sm font-medium']
      base_classes << (f.object&.errors&.key?(name) ? 'text-red-600' : 'text-gray-700')
      base_classes.join(' ')
    end

    def input_group_options
      @html
    end

    def id_for(method)
      ActionView::Base::Tags::Base.new(
        @f.object_name,
        method,
        view_context
      ).send(:tag_id)
    end

    def show_label?
      @show_label != false
    end
  end
end
