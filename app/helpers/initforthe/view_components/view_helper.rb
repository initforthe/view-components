# frozen_string_literal: true

# View Component Helper
module Initforthe
  module ViewComponents
    # View Helper
    module ViewHelper
      INITFORTHE_HELPERS = {
        avatar: 'Initforthe::AvatarComponent',
        button: 'Initforthe::ButtonComponent',
        flash: 'Initforthe::FlashComponent',
        form_errors: 'Initforthe::FormErrorsComponent',
        icon: 'Initforthe::IconComponent',
        input_group: 'Initforthe::InputGroupComponent',
        link: 'Initforthe::LinkComponent',
        modal: 'Initforthe::ModalComponent',
        navbar: 'Initforthe::NavbarComponent',
        notification_badge: 'Initforthe::NotificationBadgeComponent',
        notification: 'Initforthe::NotificationComponent',
        slideover: 'Initforthe::SlideoverComponent',
        check_box: 'Initforthe::Fields::CheckBoxComponent',
        check_box_tab: 'Initforthe::Fields::CheckBoxTabComponent',
        toggle_switch: 'Initforthe::Fields::ToggleSwitchComponent',
        country_select: 'Initforthe::Fields::CountrySelectComponent',
        date_field: 'Initforthe::Fields::DateFieldComponent',
        datetime_field: 'Initforthe::Fields::DatetimeFieldComponent',
        email_field: 'Initforthe::Fields::EmailFieldComponent',
        file_field: 'Initforthe::Fields::FileFieldComponent',
        number_field: 'Initforthe::Fields::NumberFieldComponent',
        password_field: 'Initforthe::Fields::PasswordFieldComponent',
        radio_button: 'Initforthe::Fields::RadioButtonComponent',
        rich_text_area: 'Initforthe::Fields::RichTextAreaComponent',
        search_field: 'Initforthe::Fields::SearchFieldComponent',
        select: 'Initforthe::Fields::SelectComponent',
        telephone_field: 'Initforthe::Fields::TelephoneFieldComponent',
        text_area: 'Initforthe::Fields::TextAreaComponent',
        text_field: 'Initforthe::Fields::TextFieldComponent',
        time_field: 'Initforthe::Fields::TimeFieldComponent'
      }.freeze

      INITFORTHE_HELPERS.each do |name, component|
        define_method "vc_#{name}" do |*args, **kwargs, &block|
          render component.constantize.new(*args, **kwargs), &block
        end
      end

      def vc(component, *args, **kwargs, &)
        render(component_class(component).constantize.new(*args, **kwargs), &)
      end

      private

      def component_class(name)
        "#{name.to_s.split('/').map(&:camelize).join('::')}Component"
      end
    end
  end
end
