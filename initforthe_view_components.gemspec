# frozen_string_literal: true

require_relative 'lib/initforthe/view_components/version'

Gem::Specification.new do |spec|
  spec.name        = 'initforthe_view_components'
  spec.version     = Initforthe::ViewComponents::VERSION
  spec.authors     = ['Tomislav Simnett']
  spec.email       = ['tom@initforthe.com']
  # spec.homepage    = "TODO"
  spec.summary     = 'Components Used By Initforthe'
  spec.description = 'Tailwind-based View Components for Rails'
  spec.license     = 'MIT'

  spec.required_ruby_version = '>= 3.1'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  # spec.metadata["homepage_uri"] = spec.homepage
  # spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."
  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  end

  spec.add_runtime_dependency 'active_link_to', '>= 1.0.5'
  spec.add_runtime_dependency 'country_select', '>= 8.0'
  spec.add_runtime_dependency 'font_awesome5_rails', '>= 1.5.0'
  spec.add_runtime_dependency 'heroicons', '>= 1.0.0'
  spec.add_runtime_dependency 'rails', '>= 7.0.0'
  spec.add_runtime_dependency 'view_component', '>= 3.0.0'
  spec.add_runtime_dependency 'config', '>= 4.2'

  spec.add_development_dependency 'capybara', '>= 3.39'
  spec.add_development_dependency 'cuprite', '>= 0.14'
  spec.add_development_dependency 'evil_systems', '>= 1.1.5'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'puma', '>= 6'
  spec.add_development_dependency 'rubocop', '>= 1.50'
  spec.add_development_dependency 'simplecov', '>= 0.21.2'
end
