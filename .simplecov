# frozen_string_literal: true

SimpleCov.start 'rails' do
  add_group 'Components', 'app/components'
  add_filter 'demo'
end
