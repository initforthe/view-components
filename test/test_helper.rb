# frozen_string_literal: true

require 'simplecov'

# Configure Rails Environment
ENV['RAILS_ENV'] = 'test'

require_relative '../demo/config/environment'
require 'minitest/autorun'
require 'rails'
require 'rails/test_help'
require 'test_helpers/component_test_helper'
require 'pry'
