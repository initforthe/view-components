# frozen_string_literal: true

require 'application_system_test_case'

class LinkComponentSystemTest < ApplicationSystemTestCase
  def test_basic_link
    with_preview('link_component/default')

    assert_selector('a') do
      assert_selector('svg')
    end
  end
end
