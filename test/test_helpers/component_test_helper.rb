# frozen_string_literal: true

module Initforthe
  module ComponentTestHelpers
    include ActionView::Helpers::TagHelper
    include ViewComponent::TestHelpers
    include Initforthe::ViewComponents::ViewHelper
  end
end

class User
  include ActiveModel::Model
  include ActiveModel::Validations
  attr_accessor :foo, :name, :email

  validates :foo, presence: true
end
