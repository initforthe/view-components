# frozen_string_literal: true

require 'test_helper'
require 'capybara/cuprite'

require 'evil_systems'

EvilSystems.initial_setup(skip_task: true)

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :evil_cuprite

  include EvilSystems::Helpers

  def with_preview(path)
    visit "/rails/view_components/#{path}"
  end
end
