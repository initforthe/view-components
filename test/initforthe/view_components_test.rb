# frozen_string_literal: true

require 'test_helper'

module Initforthe
  class ViewComponentsTest < ActiveSupport::TestCase
    test 'it has a version number' do
      assert Initforthe::ViewComponents::VERSION
    end
  end
end
