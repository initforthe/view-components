# frozen_string_literal: true

require 'test_helper'

class FlashComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  NOTIFICATION_TYPES = [:alert, :notice, :error, :warning, :success, nil].freeze

  def test_default_flash_component
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::FlashComponent.new(message: 'Something happened!', type:))

      assert_selector :xpath, './/flash-notification[contains(@data-controller, "delay")]', visible: false
    end
  end

  def test_flash_with_header_and_message_and_links
    message = {
      title: 'You have been logged in',
      description: 'You can now access the dashboard',
      links: [
        { text: 'Go to dashboard', url: '/dashboard' },
        { text: 'Go to profile', url: '/profile' }
      ]
    }
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::FlashComponent.new(message:, type:))

      assert_selector 'flash-notification', visible: false do
        assert_selector 'h3', text: 'You have been logged in', visible: false
        assert_selector 'p', text: 'You can now access the dashboard', visible: false
        assert_selector 'a', text: 'Go to dashboard', visible: false
        assert_selector 'a', text: 'Go to profile', visible: false
      end
    end
  end

  def test_flash_with_header_and_message_and_buttons
    message = {
      title: 'You have been logged in',
      description: 'You can now access the dashboard',
      buttons: [
        { text: 'Mark read', html: { data: { url: '/dashboard' } } }
      ]
    }
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::FlashComponent.new(message:, type:))

      assert_selector 'flash-notification', visible: false do
        assert_selector 'h3', text: 'You have been logged in', visible: false
        assert_selector 'p', text: 'You can now access the dashboard', visible: false
        assert_selector 'button', text: 'Mark read', visible: false
      end
    end
  end

  def test_flash_with_header_and_message_and_avatar
    message = {
      user: {
        name: 'Test Name',
        email: 'test@example.com'
      },
      title: 'You have been logged in',
      description: 'You can now access the dashboard'
    }
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::FlashComponent.new(message:, type:))

      assert_selector 'flash-notification', visible: false do
        assert_selector 'img[alt="Test Name"]', visible: false
        assert_selector 'h3', text: 'You have been logged in', visible: false
        assert_selector 'p', text: 'You can now access the dashboard', visible: false
      end
    end
  end
end
