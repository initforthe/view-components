# frozen_string_literal: true

require 'test_helper'

class FormErrorsComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  def test_default_form_errors_component
    user = User.new
    user.valid?
    render_inline(Initforthe::FormErrorsComponent.new(user))

    assert_selector 'div.notification' do
      assert_selector 'h3', text: '1 error stopped this user being saved:'
    end
  end
end
