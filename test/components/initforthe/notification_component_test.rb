# frozen_string_literal: true

require 'test_helper'

class NotificationComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  NOTIFICATION_TYPES = [:alert, :notice, :error, :warning, :success, nil].freeze

  def test_default_notification_component
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::NotificationComponent.new(type:)) do |component|
        component.with_header title: 'Heading World'
      end

      assert_selector 'div.notification'
      assert_selector :xpath, '//div[contains(@data-controller, "timer")]'
    end
  end

  def test_notification_with_header_and_message_and_links
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::NotificationComponent.new(type:)) do |component|
        component.with_header(title: 'Heading World')
        component.with_message { tag.p('Hello, World') }
        component.with_link { tag.a('Click Me', href: '#') }
        component.with_link { tag.a('Click You', href: '#') }
      end

      assert_selector 'div.notification' do
        assert_selector 'h3', text: 'Heading World', visible: false
        assert_selector 'p', text: 'Hello, World', visible: false
        assert_selector 'a', text: 'Click Me', visible: false
        assert_selector 'a', text: 'Click You', visible: false
      end
    end
  end

  def test_notification_with_data_hash
    data = {
      heading: 'Heading World',
      text: 'Hello, World',
      links: [
        tag.a('Click Me', href: '#'),
        tag.a('Click You', href: '#')
      ]
    }

    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::NotificationComponent.new(type:, data:))

      assert_selector 'div.notification' do
        assert_selector 'h3', text: 'Heading World', visible: false
        assert_selector 'p', text: 'Hello, World', visible: false
        assert_selector 'a', text: 'Click Me', visible: false
        assert_selector 'a', text: 'Click You', visible: false
      end
    end
  end

  def test_notification_with_data_string
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::NotificationComponent.new(type:, data: 'Hello, World'))

      assert_selector 'div.notification' do
        assert_selector 'p', text: 'Hello, World', visible: false
      end
    end
  end

  def test_dismissible_notification
    NOTIFICATION_TYPES.each do |type|
      render_inline(Initforthe::NotificationComponent.new(type:, dismissible: true, data: 'Hello, World'))

      assert_no_selector :xpath, '//div[contains(@data-controller, "timer")]'
    end
  end
end
