# frozen_string_literal: true

require 'test_helper'

class AvatarComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  def test_avatar_component
    render_inline(Initforthe::AvatarComponent.new(name: 'Test Name', email: 'test@example.com'))

    assert_selector 'img[alt="Test Name"]'
  end

  def test_avatar_component_with_custom_alt
    render_inline(Initforthe::AvatarComponent.new(name: 'Test Name', email: 'test@example.com', alt: 'Custom Alt'))

    assert_selector 'img[alt="Custom Alt"]'
  end

  def test_avatar_component_with_custom_classes
    render_inline(Initforthe::AvatarComponent.new(name: 'Test Name', email: 'test@example.com',
                                                  classes: 'custom-class'))

    assert_selector 'img.custom-class'
  end
end
