# frozen_string_literal: true

require 'test_helper'

class InputGroupComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  def test_input_group_component
    form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
    render_inline(Initforthe::InputGroupComponent.new(f: form_object, name: :foo)) do
      'Hello, World'
    end

    assert_selector 'div', text: 'Hello, World' do
      assert_selector 'label', text: 'Foo'
    end
  end

  def test_input_group_component_without_label
    form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
    render_inline(Initforthe::InputGroupComponent.new(f: form_object, name: :foo, show_label: false)) do
      'Hello, World'
    end

    assert_selector 'div', text: 'Hello, World' do
      assert_no_selector 'label'
    end
  end
end
