# frozen_string_literal: true

require 'test_helper'

class NavbarComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  def test_navbar_component
    render_inline(Initforthe::NavbarComponent.new(user: User.new(name: 'Foo', email: 'foo@bar.com')))

    assert_selector 'nav'
  end
end
