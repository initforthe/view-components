# frozen_string_literal: true

require 'test_helper'

module Fields
  class RadioButtonComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_radio_button_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::RadioButtonComponent.new(name: :foo, f: form_object, value: 'bar'))

      assert_selector 'input[type="radio"][name="user[foo]"][value="bar"]'
    end
  end
end
