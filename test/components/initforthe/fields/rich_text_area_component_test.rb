# frozen_string_literal: true

require 'test_helper'

module Fields
  class RichTextAreaComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_rich_text_area_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::RichTextAreaComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="hidden"][name="user[foo]"]', visible: false
      assert_selector 'trix-editor[id="user_foo"]'
    end
  end
end
