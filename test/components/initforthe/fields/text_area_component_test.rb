# frozen_string_literal: true

require 'test_helper'

module Fields
  class TextAreaComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_text_area_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::TextAreaComponent.new(name: :foo, f: form_object))

      assert_selector 'textarea[name="user[foo]"]'
    end
  end
end
