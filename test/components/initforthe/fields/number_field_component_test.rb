# frozen_string_literal: true

require 'test_helper'

module Fields
  class NumberFieldComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_number_field_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::NumberFieldComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="number"][name="user[foo]"]'
    end
  end
end
