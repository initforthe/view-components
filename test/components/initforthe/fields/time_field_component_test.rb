# frozen_string_literal: true

require 'test_helper'

module Fields
  class TimeFieldComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_time_field_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::TimeFieldComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="time"][name="user[foo]"]'
    end
  end
end
