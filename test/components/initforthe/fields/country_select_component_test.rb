# frozen_string_literal: true

require 'test_helper'

module Fields
  class CountrySelectComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_country_select_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::CountrySelectComponent.new(name: :foo, f: form_object))

      assert_selector 'select[name="user[foo]"]' do
        assert_selector 'option[value="AF"]'
      end
    end
  end
end
