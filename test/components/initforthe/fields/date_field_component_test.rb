# frozen_string_literal: true

require 'test_helper'

module Fields
  class DateFieldComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_date_field_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::DateFieldComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="date"][name="user[foo]"]'
    end
  end
end
