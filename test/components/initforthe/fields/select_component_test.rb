# frozen_string_literal: true

require 'test_helper'

module Fields
  class SelectComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_select_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::SelectComponent.new(name: :foo, f: form_object,
                                                            options: [%w[Afghanistan AF]]))

      assert_selector 'select[name="user[foo]"]' do
        assert_selector 'option[value="AF"]'
      end
    end
  end
end
