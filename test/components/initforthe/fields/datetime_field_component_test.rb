# frozen_string_literal: true

require 'test_helper'

module Fields
  class DatetimeFieldComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_datetime_field_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::DatetimeFieldComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="datetime-local"][name="user[foo]"]'
    end
  end
end
