# frozen_string_literal: true

require 'test_helper'

module Fields
  class CheckBoxComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_check_box_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::CheckBoxComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="checkbox"][name="user[foo]"]'
    end
  end
end
