# frozen_string_literal: true

require 'test_helper'

module Fields
  class SearchFieldComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_search_field_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::SearchFieldComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="search"][name="user[foo]"]'
    end
  end
end
