# frozen_string_literal: true

require 'test_helper'

module Fields
  class TextFieldComponentTest < Minitest::Test
    include Initforthe::ComponentTestHelpers

    def test_text_field_component
      form_object = ActionView::Helpers::FormBuilder.new(:user, User.new, vc_test_controller.view_context, {})
      render_inline(Initforthe::Fields::TextFieldComponent.new(name: :foo, f: form_object))

      assert_selector 'input[type="text"][name="user[foo]"]'
    end
  end
end
