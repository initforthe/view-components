# frozen_string_literal: true

require 'test_helper'

class SlideoverComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  def test_default_slideover_component
    render_inline(Initforthe::SlideoverComponent.new(heading: 'Heading World')) do |_component|
      'Hello, World'
    end

    assert_selector 'div[data-slideover-container]', text: 'Hello, World', visible: false do
      assert_selector 'h2', text: 'Heading World', visible: false
      assert_selector 'div.max-w-md', visible: false
    end
  end
end
