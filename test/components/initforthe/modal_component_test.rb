# frozen_string_literal: true

require 'test_helper'

class ModalComponentTest < Minitest::Test
  include Initforthe::ComponentTestHelpers

  def test_default_modal_component
    render_inline(Initforthe::ModalComponent.new) { 'Hello, World!' }

    assert_selector 'div[data-modal-container]', text: 'Hello, World!' do
      assert_selector 'div.sm\:max-w-sm', visible: false
    end
  end

  def test_tag_options_on_modal_component
    render_inline(Initforthe::ModalComponent.new(class: 'some-class', data: { foo: true })) { 'Hello, World!' }

    assert_selector 'div.some-class[data-foo]', text: 'Hello, World!'
  end

  def test_controllers_on_modal_component
    render_inline(Initforthe::ModalComponent.new(controllers: 'foo', actions: 'foo->foo#foo'))

    assert_selector :xpath, './/div[contains(@data-controller, "foo")]'
    assert_selector :xpath, './/div[contains(@data-action, "foo->foo#foo")]'
  end

  def test_modal_component_size
    render_inline(Initforthe::ModalComponent.new(size: 'sm:max-w-lg')) { 'Hello, World!' }

    assert_selector 'div[data-modal-container]', text: 'Hello, World!' do
      assert_no_selector 'div.sm\:max-w-sm', visible: false
      assert_selector 'div.sm\:max-w-lg', visible: false
    end
  end

  def test_modal_dismissible
    render_inline(Initforthe::ModalComponent.new(dismissible: true))

    assert_selector 'div[data-modal-container]' do
      assert_selector 'div', visible: false do
        assert_selector 'button', text: 'Close', visible: false
      end
    end
  end

  def test_modal_overflow
    render_inline(Initforthe::ModalComponent.new(overflow: true))

    assert_no_selector 'div.overflow-hidden', visible: false
  end
end
